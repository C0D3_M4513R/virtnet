package com.textadventure.virtnet.springboot;

import com.textadventure.virtnet.db.*;
import com.textadventure.virtnet.service.UI;
import com.textadventure.virtnet.service.World;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GameClass {
	
	
	User user;
	World world;
	@Autowired
	UI ui;
	@Autowired
	private RoomRepository roomRepo;
	@Autowired
	private ItemRepository itemRepo;
	@Autowired
	private ItemRoomRepository eventRepo;
	@Autowired
	private InventoryRepository inventoryRepo;
	@Autowired
	private UserRepository userRepo;
	String roomDesc ="";
	String map ="";
	
	@RequestMapping ("/play")
	public String gamePlay (Authentication auth, Model model) {
		user = userRepo.findByUsername(auth.getName());
		
		if (roomRepo.findAll() == null) {
			throw new NullPointerException("Please add a Room");
		}
		if (itemRepo.findAll() == null) {
			throw new NullPointerException("Please add items");
		}
		if (eventRepo.findAll() == null) {
			throw new NullPointerException("Please add events (itemRooms)");
		}
		
		
		world = new World(user, roomRepo.findAll(), itemRepo.findAll());
		ui.setPlayer(user);
		ui.setWorld(world);
		model.addAttribute("map", ui.menu("map"));
		model.addAttribute("commandHistory", "> help \n " + ui.menu("help"));
		model.addAttribute("roomDesc", getRoomDesc());
		model.addAttribute("intro",UI.intro());
		
		return "play";
	}
	
	private String commandExecuter(String cmd,int i){
		try{
		if(i<50){
			String tmp = ui.menu(cmd);
			System.out.println("tmp = " + tmp);
			if(tmp!=null) {
				if(cmd.equals("map") && !tmp.equals(map))
					return tmp;
				if (!cmd.equals("map"))
					return tmp;
				return commandExecuter(cmd,+i);
			}
			return commandExecuter(cmd,+i);
		}
		return null;
	} catch (StackOverflowError e){
		return ui.menu(cmd);
	}
	}
	private String roomDescs(int i){
		try {
			if (i < 50) {
				String tmp = ui.getRoomDesc();
				System.out.println("tmp = " + tmp);
				if (tmp != null && !tmp.equals(roomDesc)) {
					roomDesc = tmp;
					return tmp;
				}
				return roomDescs(+i);
			}
			return null;
		} catch (StackOverflowError e){
			return ui.getRoomDesc();
		}
	}
	
	@PostMapping ("/roomDesc")
	public @ResponseBody
	String getRoomDesc () {
		return roomDescs(0);
	}
	
	@PostMapping ("/command")
	public @ResponseBody
	String getCommandOutput (@RequestParam String command) {
		return commandExecuter(command,0);
	}
	@PostMapping("/map")
	public @ResponseBody String getMap(){
		return commandExecuter("map",0);
	}
	
	
}
