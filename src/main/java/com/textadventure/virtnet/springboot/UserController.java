package com.textadventure.virtnet.springboot;

import com.textadventure.virtnet.db.User;
import com.textadventure.virtnet.db.UserRepository;
import com.textadventure.virtnet.service.Alert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import java.net.URLDecoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class UserController {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UserRepository userRepository;
	
	private final static String error = "alert alert-danger alert-dismissible fade in show";
	
	
	private HashSet<Alert> passwordValidator(String pass, String pass2){
		HashSet<Alert> alerts = new HashSet<>();
		if (!pass.equals(pass2)) {
			alerts.add(new Alert("Passwords don't match", error));
		}
		if (pass.length() <= 8) {
			alerts.add(new Alert("Password is too short. <br>It has to be longer than 8 characters.", error));
		}
		Matcher subMatcher = Pattern.compile("[A-Z]").matcher(pass);
		if (!subMatcher.find()) {
			alerts.add(new Alert("Add at least one Uppercase letter", error));
		}
		subMatcher = Pattern.compile("[a-z]").matcher(pass);
		if (!subMatcher.find()) {
			alerts.add(new Alert("Add at least one Lowercase letter", error));
		}
		subMatcher = Pattern.compile("[0-9]").matcher(pass);
		if (!subMatcher.find()) {
			alerts.add(new Alert("Add at least one number", error));
		}
		subMatcher = Pattern.compile("\\S").matcher(pass);
		if (!subMatcher.find()) {
			alerts.add(new Alert("Add at least one special character", error));
		}
		return alerts;
	}
	
	@GetMapping("/register")
	public String registerView(Model model,Authentication auth){
		if (auth != null && auth.isAuthenticated()) return "main";
		
		model.addAttribute("title","Register now to play!");
		return "register";
	}
	
	@PostMapping("/register")
	public String register(Model model,
	                       @RequestParam String username,
	                       @RequestParam String password,
	                       @RequestParam String confirm) throws JSONException {
		
		System.out.println(" username = [" + username + "], password = [" + password + "], confirm = [" + confirm + "]");
		
		HashSet<Alert> alerts = new HashSet<>();
		if(userRepository.findByUsername(username)!=null) {
			alerts.add(new Alert("Username already in use!",error));
			model.addAttribute("alerts",alerts);
		return "register";
		}
		alerts.addAll(passwordValidator(password,confirm));
		if (alerts.size() == 0) {
			User user = new User();
			user.setPassword(passwordEncoder.encode(password));
			user.setUsername(username);
			user.setActive(true);
			userRepository.save(user);
			alerts.add(new Alert("Successfully changed password.", "alert alert-success alert-dismissible fade in show"));
			model.addAttribute("script", "history.pushState(null,null,\"play\");");
			model.addAttribute("alerts", alerts);
			return "main";
		}
		
		model.addAttribute("alerts",alerts);
		
		JSONArray json = new JSONArray();
		
		Iterator<Alert> iterator = alerts.iterator();
		while (iterator.hasNext()){
			Alert alert = iterator.next();
			json.put(alert.alertToJson());
		}
		
		System.out.println(json.toString());

		return "register";
	
	}
	
	
	
	@PostMapping ("/changepw")
	public String pwToDB (@RequestParam String oldPasswd,
	                      @RequestParam String newPasswd,
	                      @RequestParam String newPasswd1,
	                      Authentication auth,
	                      Model model) {
		
		User user = userRepository.findByUsername(auth.getName());
		HashSet<Alert> alerts = new HashSet<>();
		if (passwordEncoder.matches(oldPasswd, user.getPassword())) {//check if passwords are the same
			alerts = passwordValidator(newPasswd,newPasswd1);
			if (alerts.size() == 0) {
				user.setPassword(passwordEncoder.encode(newPasswd));
				userRepository.save(user);
				alerts.add(new Alert("Successfully changed password.", "alert alert-success alert-dismissible fade in show"));
				model.addAttribute("script", "history.pushState(null,null,\"user\");");
				model.addAttribute("alerts", alerts);
				return "main";
			}
		} else {
			alerts.add(new Alert("The old password is wrong. <br>Please try again.", error));
		}
		
		model.addAttribute("alerts", alerts);
		return this.changePW(model, auth);
	}
	
	
	@GetMapping ("/changepw")
	public String changePW (Model model, Authentication auth) {
		
		model.addAttribute("title","Change Passwords.");
		model.addAttribute("username", auth.getName());
		return "changepw";
	}
	
	//begin debug mappings
	@GetMapping (path = "/add") // Map ONLY GET Requests
	public @ResponseBody
	String addNewUser (@RequestParam String name
			, @RequestParam String pass
			, @RequestHeader HttpHeaders header) {
		
		String host = Objects.requireNonNull(header.getHost()).getHostName();
		if (host.equals("localhost") || host.equals("127.0.0.1")) {
			// @ResponseBody means the returned String is the response, not a view name
			// @RequestParam means it is a parameter from the GET or POST request
			
			User n = new User();
			n.setUsername(name);
			n.setPassword(passwordEncoder.encode(pass));
			
			userRepository.save(n);
			
			
			return "Saved";
		}
		throw new AccessDeniedException("Host is not localhost");
	}
	
	@GetMapping (path = "/all/users")
	public @ResponseBody
	String getAllUsers (@RequestHeader HttpHeaders header) throws JSONException {
		
		String host = Objects.requireNonNull(header.getHost()).getHostName();
		if (host.equals("localhost") || host.equals("127.0.0.1")) {
			// This returns a JSON or XML with the users
			JSONArray json = new JSONArray();
			
			for (User user : userRepository.findAll()) {
				json.put(user.userToJson());
			}
			return json.toString(4);
		}
		throw new AccessDeniedException("Host is not localhost");
	}
	
	@RequestMapping (path = "/kill")
	public @ResponseBody
	String kill (@RequestHeader HttpHeaders header) {
		String host = Objects.requireNonNull(header.getHost()).getHostName();
		if (host.equals("localhost") || host.equals("127.0.0.1")) {
			System.exit(0);
			return "Successful failure!";
		}
		throw new AccessDeniedException("Host is not localhost");
	}
}
