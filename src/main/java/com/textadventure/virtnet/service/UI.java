package com.textadventure.virtnet.service;

import com.textadventure.virtnet.db.InventoryRepository;
import com.textadventure.virtnet.db.User;
import com.textadventure.virtnet.db.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.StringTokenizer;

/**
 * The UI class provides the User Interface in which the player will
 * interact with while playing the game game via input and output.
 *
 * @author Jeremy Wolff
 * @version 1.0
 */
@Service
public class UI {
	public static boolean done = false;
	/**
	 *
	 */
	@Getter
	@Setter
	World world;
	@Getter
	@Setter
	User player;
	
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private InventoryRepository invRepo;
	
	/**
	 * Constructor for a UI object.
	 *
	 * @param gamePlayer
	 * 		the player made in the Game class is passed into this object- mostly for convenience.
	 * @param gameWorld
	 * 		the world made in the Game class is passed into this object- mostly for convenience.
	 */
	public UI (User gamePlayer, World gameWorld) {
		player = gamePlayer;
		world = gameWorld;
	}
	
	/**
	 * Retrieve the introductory text shown to the player at the beginning of the game.
	 *
	 * @return The introductor text as a string.
	 */
	public static String intro () {
		String introText = "";
		// ASCII Font found courtesy of http://patorjk.com/software/taag
		// NOTE: If your font has any \'s in them, they will need to be "escaped".
		
		introText += " __  __             __    __  __          __      \n";
		introText += "/\\ \\/\\ \\  __       /\\ \\__/\\ \\/\\ \\        /\\ \\__   \n";
		introText += "\\ \\ \\ \\ \\/\\_\\  _ __\\ \\ ,_\\ \\ `\\\\ \\     __\\ \\ ,_\\  \n";
		introText += " \\ \\ \\ \\ \\/\\ \\/\\`'__\\ \\ \\/\\ \\ , ` \\  /'__`\\ \\ \\/  \n";
		introText += "  \\ \\ \\_/ \\ \\ \\ \\ \\/ \\ \\ \\_\\ \\ \\`\\ \\/\\  __/\\ \\ \\_ \n";
		introText += "   \\ `\\___/\\ \\_\\ \\_\\  \\ \\__\\\\ \\_\\ \\_\\ \\____\\\\ \\__\\\n";
		introText += "    `\\/__/  \\/_/\\/_/   \\/__/ \\/_/\\/_/\\/____/ \\/__/\n";
		introText += "                                                  \n";
		introText += "                                                  \n";
		
		return introText;
	}
	
	/**
	 * Menu in which the player interacts with. Handles a few different aspects of the game:
	 * <ol>
	 * <li>Prints out introduction text.</li>
	 * <li>Prints out the details of the room.</li>
	 * <li>Interprets the player's input text and executes the associated commands.</li>
	 * </ol>
	 */
	public String menu (String commandString) {
		
		StringBuilder sb = new StringBuilder();
		
		// Describe the room to the player
		world.updateNearbyRoomsMap(player.getCurrentRoom());
		
		// Get the command and [optional] argument as lowercase strings
		StringTokenizer parser = new StringTokenizer(commandString, "\t,.;:?! ");
		String command = getCommand(parser);
		String argument = getArgument(parser);
		
		String tmp = parseInput(command, argument);
		sb.append(tmp);
		
		userRepo.saveAndFlush(player);
		invRepo.saveAll(player.getInventory());
		
		
		return sb.toString();
	}
	
	private String getCommand (StringTokenizer parser) {
		String command = "";
		if (parser.hasMoreTokens()) {
			command = parser.nextToken().toLowerCase();
		}
		return command;
	}
	
	private String getArgument (StringTokenizer parser) {
		String argument = "";
		while (parser.hasMoreTokens()) {
			argument += parser.nextToken();
			if (parser.hasMoreTokens()) {
				argument += " ";
			}
		}
		return argument;
	}
	
	private String parseInput (String command, String argument) {
		// Call the appropriate command of the Player. If you add new commands,
		// add a test for the new command name here and make the appropriate
		// call(s) to the Player.
		switch (command) {
			case "help":
				return help();
			case "take":
				return player.tryToTakeItem(argument);
			case "use":
				return player.tryToUseItem(argument,world);
			case "inventory":
			case "i":
				return player.outputInventory();
			case "go":
				return player.tryToMove(argument.toLowerCase(),world);
			case "n":
			case "north":
			case "e":
			case "east":
			case "s":
			case "south":
			case "w":
			case "west":
				return player.tryToMove(command,world);
			case "map":
				world.updateNearbyRoomsMap(player.getCurrentRoom());
				return world.printNearbyRoomsMap();
			case "q":
			case "quit":
				break;
			default:
				return "Invalid option. Use help if you're not sure what you can do.\n>\t";
		}
		return null;
	}
	
	/**
	 * Returns a list of available commands.
	 *
	 * @return The list of available commands.
	 */
	private static String help () {
		String commands = "\nAVAILABLE COMMANDS: \n";
		commands += " - help\n";
		commands += " - go <DIRECTION>, n, s, e, w\n";
		commands += " - map\n";
		commands += " - take <ITEM>\n";
		commands += " - use <ITEM>\n";
		commands += " - inventory or i\n";
		commands += " - quit or q\n";
		return commands;
	}
	
	public String getRoomDesc () {
		return world.getRoomDetails(player.getCurrentRoom());
	}
	
}