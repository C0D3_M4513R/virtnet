package com.textadventure.virtnet.service;

import com.textadventure.virtnet.db.*;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class represents the whole world. It constructs the rooms and gives us the ability
 * to get details of a room, check if the user can move into another room, find out if a room exists, retrieve a room.
 * <p>
 * This class also allows us to construct any map for the game world.
 * <p>
 * The World class has functions to display the nearby rooms in printed format.
 *
 * @author Jeremy Wolff
 * @version 1.0
 */
@Service
public class World {
	@Getter
	static List<Room> map;
	@Getter
	static List<Item> items;
	static String[] nearbyRoomsMap = new String[9];
	@Getter
	User user;
	
	
	/**
	 * Constructor for objects of class World.
	 * This constructor calls methods to:
	 * <ol>
	 * <li>populate the map in the world</li>
	 * <li>populate the items and place them into their appropriate rooms</li>
	 * </ol>
	 *
	 * @param user
	 * 		The user in the game.
	 */
	public World (User user, List<Room> rooms, List<Item> items1) {
		//map = myMap();
		map = rooms;
		items = items1;
		this.user = user;
	}
	
	
	/**
	 * Search through our map to find a specific room
	 *
	 * @param row
	 * 		row of the room that we're trying to find.
	 * @param column
	 * 		column of the room that we're trying to find.
	 *
	 * @return true if there is room at the indicated row and column.
	 */
	public boolean roomExists (int row, int column,int worldid) {
		return getRoom(row, column,worldid) != null;
	}
	
	/**
	 * Retrive a room in the map at a specified location
	 *
	 * @param currentRow
	 * 		row of the room being queried
	 * @param currentColumn
	 * 		column of the room being queried
	 *
	 * @return the room that exists at this row and column.
	 * returns null if there's no room at this location.
	 */
	public Room getRoom (int currentRow, int currentColumn,int worldid) {
		if (map != null) {
			for (Room room : map) {
				if (room.getRow() == currentRow && room.getColumn() == currentColumn && worldid == room.getWorld().getId()) {
					return room;
				}
			}
			return null;
		}
		throw new NullPointerException("Room map is null");
	}
	
	/**
	 * Retrive a room in the map by its name
	 *
	 * @param name
	 * 		the name of the room being searched for.
	 *
	 * @return the room object, if the room exists.
	 */
	public static Room getRoom (String name) {
		
		for (Room room : map)
			if (room.getName().equals(name)) return room;
		return null;
	}
	
	/**
	 * Allows us to get the total number of rooms in the map.
	 *
	 * @return the number of rooms in the map.
	 */
	public int getNumRooms () {
		return map.size();
	}
	
	/**
	 * Gives the details about a room. This function also adds directions to the nearybyRoomMap.
	 *
	 * @param room
	 * 		a reference to the room that the user is seeking details about.
	 *
	 * @return a full description of the room and all possible actions directions the user can move
	 */
	public String getRoomDetails (Room room) {
		String details = "";
		details += "\n----------------------------------------------------------\n";
		details += room.getDescription(user);
		details += "\n";
		details += "You may go";
		
		// Check each direction and add that direction to details if there's a room there.
		details += getDirectionDetails(room).isEmpty()?" nowhere.":getDirectionDetails(room);
		
		return details;
	}
	
	private String getDirectionDetails (Room room) {
		String details = "";
		Connections connections = room.getConnections();
		if (connections.isNorth() && isRoomInDirection(room,"n")) {
			details += " [North]";
		}
		if (connections.isSouth()  && isRoomInDirection(room,"s")) {
			details += " [South]";
		}
		if (connections.isEast()  && isRoomInDirection(room,"e")) {
			details += " [East]";
		}
		if (connections.isWest()  && isRoomInDirection(room,"w")) {
			details += " [West]";
		}
		return details;
	}
	
	private boolean isRoomInDirection(Room room,String dicection){
		int currentRow = room.getRow();
		int currentColumn = room.getColumn();
		
		switch (dicection.toLowerCase().charAt(0)){
			case 'n':
				currentRow-=1;
				break;
			case 's':
				currentRow+=1;
				break;
			case 'w':
				currentColumn-=1;
				break;
			case 'e':
				currentColumn+=1;
				break;
			default:
				return false;
		}
		return roomExists(currentRow,currentColumn,room.getWorld().getId());
	}
	
	public void updateNearbyRoomsMap (Room room) {
		resetNearbyRoomsMap();
		Connections connections = room.getConnections();
		if (connections.isNorth() && isRoomInDirection(room,"n")) {
			addNearbyRoom("n");
		}
		if (connections.isSouth() && isRoomInDirection(room,"s")) {
			addNearbyRoom("s");
		}
		if (connections.isEast() && isRoomInDirection(room,"e")) {
			addNearbyRoom("e");
		}
		if (connections.isWest() && isRoomInDirection(room,"w")) {
			addNearbyRoom("w");
		}
	}
	
	
	/**
	 * Sets the mearbyRoomsMap back to its default value so rooms can be added to its sides.
	 */
	private void resetNearbyRoomsMap () {
		nearbyRoomsMap[0] = "************\n";
		nearbyRoomsMap[1] = "************\n";
		nearbyRoomsMap[2] = "************\n";
		nearbyRoomsMap[3] = "****    ****\n";
		nearbyRoomsMap[4] = "**** @@ ****\n";
		nearbyRoomsMap[5] = "****    ****\n";
		nearbyRoomsMap[6] = "************\n";
		nearbyRoomsMap[7] = "************\n";
		nearbyRoomsMap[8] = "************\n";
	}
	
	/**
	 * When the description of a room is provided, the addNearbyRoom function is called for each
	 * direction that is available to the user. This allows the user to have the most current
	 * nearbyRoomMap available.
	 *
	 * @param direction
	 * 		The direction that needs to be added to the map.
	 */
	private void addNearbyRoom (String direction) {
		if (direction.equals("e")) {
			nearbyRoomsMap[2] = nearbyRoomsMap[2].substring(0, 8) + "****\n";
			nearbyRoomsMap[3] = nearbyRoomsMap[3].substring(0, 8) + "    \n";
			nearbyRoomsMap[4] = nearbyRoomsMap[4].substring(0, 8) + "    \n";
			nearbyRoomsMap[5] = nearbyRoomsMap[5].substring(0, 8) + "    \n";
			nearbyRoomsMap[6] = nearbyRoomsMap[6].substring(0, 8) + "****\n";
		}
		if (direction.equals("w")) {
			nearbyRoomsMap[2] = "****" + nearbyRoomsMap[2].substring(4);
			nearbyRoomsMap[3] = "    " + nearbyRoomsMap[3].substring(4);
			nearbyRoomsMap[4] = "    " + nearbyRoomsMap[4].substring(4);
			nearbyRoomsMap[5] = "    " + nearbyRoomsMap[5].substring(4);
			nearbyRoomsMap[6] = "****" + nearbyRoomsMap[6].substring(4);
		}
		if (direction.equals("s")) {
			nearbyRoomsMap[6] = nearbyRoomsMap[2].substring(0, 3) + "*    *" + nearbyRoomsMap[2].substring(9);
			nearbyRoomsMap[7] = "****    ****\n";
			nearbyRoomsMap[8] = "****    ****\n";
		}
		if (direction.equals("n")) {
			nearbyRoomsMap[0] = "****    ****\n";
			nearbyRoomsMap[1] = "****    ****\n";
			nearbyRoomsMap[2] = nearbyRoomsMap[2].substring(0, 3) + "*    *" + nearbyRoomsMap[2].substring(9);
		}
	}
	
	/**
	 * Print out the nearbyRoomsMap, line by line.
	 */
	public String printNearbyRoomsMap () {
		StringBuilder sb = new StringBuilder();
		for (String line : nearbyRoomsMap) {
			sb.append(line);
		}
		return sb.toString();
	}
	
}
