package com.textadventure.virtnet.service;

import lombok.Data;
import lombok.NonNull;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

@Data
public class Alert {
    @NonNull
    private String text;

    @NonNull
    private String classes;
    
    
    public JSONObject alertToJson() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("text",text).put("classes",classes);
        return json;
    }
}
