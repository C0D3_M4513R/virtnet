package com.textadventure.virtnet.db;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Iterator;
import java.util.Set;


/**
 * A Room is a "location" in the adventure game. A room has a name,
 * a description, a column, and a row.
 *
 * @version 1.0
 */
@Entity (name = "rooms")
@Service
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Room {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String desc;
	
	@ManyToOne
	@JoinColumn(name = "worldID",referencedColumnName = "id")
	private World world;
	
	@Column(name = "roow")
	private int row;
	private int column;
	
	@OneToOne
	@JoinColumn (name = "connections")
	@Setter (AccessLevel.NONE)
	private Connections connections;
	
	
	@JsonManagedReference
	@OneToMany (mappedBy = "room",fetch = FetchType.EAGER)
	private Set<ItemRoom> items;
	
	@OneToMany (mappedBy = "currentRoom")
	private Set<User> users;
	
	
	/**
	 * Returns the description of this room.
	 *
	 * @return the description of this room.
	 */
	public String getDescription (User user) {
		StringBuilder combinedDescription = new StringBuilder();
		combinedDescription.append(desc);
		
		for (ItemRoom item : items) {
			if (item == null)
				throw new NullPointerException("item can't be null!");
			if (!isItemTaken(item, user)) {
				if (item.getItemDesc() != null) {
					combinedDescription.append("\n").append(item.getItemDesc());
				} else if (!item.getItem().getItemDesc().isEmpty())
					combinedDescription.append("\n").append(item.getItem().getItemDesc());
			}
		}
		return combinedDescription.toString();
	}
	
	private Boolean isItemTaken (ItemRoom item, User user) {
		for (Inventory inventory : item.getInventory()) {
			if (inventory.getUser().equals(user))
				return true;
		}
		return false;
	}
	
	
	/**
	 * Adds an item to this room's item list.
	 *
	 * @param item
	 * 		The item to be added.
	 *
	 * @author Jeremy Wolff
	 */
	public void addItem (Item item) {
		this.items.add(new ItemRoom(item, this));
	}
	
	public void addItem (Item item, String specificDesc) {
		ItemRoom itemRoom = new ItemRoom(item, this);
		itemRoom.setItemDesc(specificDesc);
		
		this.items.add(itemRoom);
	}
	
	/**
	 * Removes an item from this room's item list.
	 *
	 * @param item
	 * 		The item to be removed.
	 *
	 * @author Jeremy Wolff
	 */
	public void removeItem (Item item) {
		for (ItemRoom itemRoom : items) {
			if (itemRoom.getItem().equals(item)) {
				items.remove(itemRoom);
			}
		}
	}
	
	public void removeItem (ItemRoom itemRoom) {
		items.remove(itemRoom);
	}
	
	
	public JSONObject roomToJson (boolean User) throws JSONException {
		return roomToJson().put("items", itemsToJson(false));
	}
	
	public JSONObject roomToJson () {
		try {
			return new JSONObject().put("Id", this.id)
					.put("Name", name)
					.put("Description", desc)
					.put("Row", row)
					.put("Column", column);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private JSONObject itemsToJson (boolean Roles) throws JSONException {
		Iterator<ItemRoom> items = this.items.iterator();
		JSONObject json = new JSONObject();
		JSONArray jsonItems = new JSONArray();
		while (items.hasNext()) {
			Item item = items.next().getItem();
			jsonItems.put(item.itemToJson());
		}
		json.put("Items", jsonItems);
		return json;
	}
	
}
