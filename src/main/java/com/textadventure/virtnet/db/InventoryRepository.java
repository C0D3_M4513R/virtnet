package com.textadventure.virtnet.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//auto implemented by springboot
@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {
	Inventory findByItemRoom(ItemRoom itemRoom);
	Inventory findByUser(User user);
}