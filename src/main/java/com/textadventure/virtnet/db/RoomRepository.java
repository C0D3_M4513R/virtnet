package com.textadventure.virtnet.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//auto implemented by springboot
@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {
	Room findByName(String name);
}
