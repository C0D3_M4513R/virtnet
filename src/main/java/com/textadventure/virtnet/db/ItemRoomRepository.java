package com.textadventure.virtnet.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//auto implemented by springboot
@Repository
public interface ItemRoomRepository extends JpaRepository<ItemRoom, Integer> {
	List<ItemRoom> findAllByItem(Item item);
	List<ItemRoom> findAllByRoom(Room room);
	ItemRoom findByItemContainingAndRoom(Item item, Room room);
}