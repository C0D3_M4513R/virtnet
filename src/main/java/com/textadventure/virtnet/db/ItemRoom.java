package com.textadventure.virtnet.db;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter @Setter @RequiredArgsConstructor @AllArgsConstructor @NoArgsConstructor
@Entity(name = "itemRoomTriggerEvent")
public class ItemRoom {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	
	@ManyToOne
	@NonNull
	private Item item;
	
	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_rooms_item-room-trigger-event"))
	@NonNull
	private Room room;
	
	@OneToMany(mappedBy = "itemRoom",fetch = FetchType.EAGER)
	private Set<Inventory> inventory;
	
	private String itemDesc;
	private Boolean takeabale;
	
}
