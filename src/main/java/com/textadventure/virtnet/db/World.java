package com.textadventure.virtnet.db;


import lombok.Getter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Entity(name = "world")
public class World {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String desc;
	
	@OneToMany(mappedBy = "world")
	private Set<Room> rooms;
}
