package com.textadventure.virtnet.db;

import liquibase.util.csv.CSVReader;
import liquibase.util.csv.opencsv.CSVParser;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Set;

@Getter @Setter
@Entity(name="useAction")
public class UseAction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String action;
	private String params;
	
	public String[] getAtr(){
		return params.split(",");
	}
	
	public void addAttr(String attr){
		params=params.concat(","+attr);
	}
	public void removeAttr(String attr){
		params = params.replace(attr,"").replace(",,",",");
	}
	
	@OneToMany(mappedBy = "useAction")
	private Set<Item> items;
	
	
}
