package com.textadventure.virtnet.db;

import com.textadventure.virtnet.service.World;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter


@Entity
@Service
public class User {
	
	//Values
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotEmpty (message = "Please enter a Username")
	private String username;
	
	@Transient
	private final String name = username;
	
	@NotEmpty (message = "Please enter a Password")
	private String password;
	private boolean active;
	private int hp;
	
	//References
	
	@ManyToOne
	@JoinColumn (name = "currentRoom", referencedColumnName = "id", foreignKey = @ForeignKey (name = "fk_User_Room"))
	private Room currentRoom;
	
	@OneToMany (mappedBy = "user", fetch = FetchType.EAGER)
	private Set<Inventory> inventory;
	
	public JSONObject userToJson () throws JSONException {
		return new JSONObject().put("Id", this.id)
				.put("Username", this.username)
				.put("Password", this.password)
				.put("Active", this.active);
	}
	
	public String tryToTakeItem (String itemName) {
		boolean foundItem = false;
		ItemRoom itemToRemove = null;
		
		// Loop through each item in the current room's item list to find the item specified.
		for (ItemRoom itemRoom : currentRoom.getItems()) {
			Item item = itemRoom.getItem();
			if (itemName.equals(item.getName())) {
				for (ItemRoom ir : item.getItemRoom()) {
					if (ir.getItem().equals(item)) {
						if ((ir.getTakeabale() == null && !item.isTakeabale()) || (!ir.getTakeabale()))
							return "You can't take " + itemName + "\n";
					}
					
				}
				
				addItemToInventory(itemRoom);
				itemToRemove = itemRoom;
				String tmp = "You grabbed the " + item.getName() + "\n";
				getCurrentRoom().removeItem(itemToRemove);
				tmp = tmp.concat("The item " + itemName + " was successfully removed!\n");
				return tmp;
			}
		}
		return ("The item " + itemName + " was not found.\n");
	}
	
	public void addItemToInventory (ItemRoom itemRoom) {
		Inventory inv = new Inventory(this, itemRoom);
		if (inventory == null)
			inventory = new HashSet<>();
		inventory.add(inv);
	}
	
	/**
	 * If the item actually exists, then we attempt to activate the item event via the item's useItem function.
	 *
	 * @param itemName
	 * 		The name of the item that the player tried to use.
	 */
	public String tryToUseItem (String itemName, World world) {
		Item item = findItem(itemName);
		if (!(item == null)) {
			return item.useItem(getCurrentRoom(), this, world);
		} else {
			for (ItemRoom ir:getCurrentRoom().getItems()){
				if(ir.getItem().getName().equals(itemName))
					return ir.getItem().useItem(getCurrentRoom(), this, world);
			}
			return ("Sorry you don't have " + itemName + "\n");
		}
	}
	
	
	/**
	 * Finds a specific item in the player's inventory.
	 *
	 * @param itemName
	 * 		item to find.
	 *
	 * @return Item, if found. If not found, then null.
	 */
	public Item findItem (String itemName) {
		for (Inventory inv : inventory) {
			Item item = inv.getItemRoom().getItem();
			if (item.getName().equals(itemName)) {
				return item;
			}
		}
		return null;
	}
	
	/**
	 * Loop through each item in the player's inventory and add the item name to a temporary variable, inventoryText. Then print out the inventoryText.
	 *
	 * @return Inventory
	 */
	public String outputInventory () {
		StringBuilder inventoryText = new StringBuilder();
		inventoryText.append("ITEMS IN YOUR INVENTORY\n");
		for (Inventory inv : getInventory()) {
			Item item = inv.getItemRoom().getItem();
			inventoryText.append(" ").append(item.getName()).append("\n");
		}
		return inventoryText.toString();
	}
	
	/**
	 * Try to move the player in the direction that the player has indicated.
	 *
	 * @param direction
	 * 		A reference to the direction that the user is moving
	 *
	 * @return true if the move was successful.
	 */
	public String tryToMove (String direction, World world) {
		int currentRow = getCurrentRoom().getRow();
		int currentColumn = getCurrentRoom().getColumn();
		Room initRoom = getCurrentRoom();
		
		int newRow = currentRow;
		int newColumn = currentColumn;
		switch (direction.charAt(0)) {
			case 'n':
				newRow -= 1;
				break;
			case 's':
				newRow += 1;
				break;
			case 'w':
				newColumn -= 1;
				break;
			case 'e':
				newColumn += 1;
				break;
		}
		Room room = world.getRoom(newRow, newColumn, initRoom.getWorld().getId());
		
		if (room != null) {
			setCurrentRoom(room);
			return "Moved to room " + room.getName() + "\n";
		}
		return "Failed to move " + direction + "\n";
	}
	
	public void setCurrentRoom (Room room) {
		System.out.println("Setting new Room");
		this.currentRoom = room;
	}
}