package com.textadventure.virtnet.db;

import com.textadventure.virtnet.service.World;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Iterator;
import java.util.Set;

/**
 * The item class allows the player to pick up and use items in the game.
 * This can be helpful for utilizing keys, granting players special options,
 * or generally allowing players to get more information from things in a room
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "items")
@Service
public class Item {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String itemDesc;
	private boolean takeabale;
	
	@OneToMany(mappedBy = "item",fetch = FetchType.EAGER)
	private Set<ItemRoom> itemRoom;
	
	@ManyToOne
	@JoinColumn(name = "useActionId")
	private UseAction useAction;
	
	
	/**
	 * @param room The player's current Room
	 */
	public String useItem(Room room, User user, World world) {
		for (ItemRoom ir : itemRoom) {
			if (ir.getRoom().equals(room)) {
				if(useAction!=null)
					switch (useAction.getAction()){
						case "teleport":
							Room newRoom = world.getRoom(0,0,Integer.parseInt(useAction.getAtr()[0]));
							 user.setCurrentRoom(newRoom);
							 return "You Teleported to the "+ newRoom.getWorld().getName()+"\n"+
									 newRoom.getWorld().getDesc();
									 
					}
				return String.format("Successfully used %s \n", name.toUpperCase());
			}
		}
		return String.format("You can't use %s right now \n", name.toUpperCase());
	}
	
	/*
	/**
	 * The player has attempted to use this item.
	 * This method goes through our list of events and sees if one of the events' conditions are met.
	 * The conditions of an event include an item and a specific room.
	 * @param room The room that the player is currently in.
	 
	public void useItem(Room room) {
		for (Event event : World.events) {
			if (event.getRoom() == room && event.getItem() == this) {
				event.triggerEvent();
				return;
			}
		}
		System.out.print("You can't use that item right now.\n");
	}
	*/
	
	
	public JSONObject itemToJson(boolean Role) throws JSONException {
		
		return itemToJson().put("Rooms", roomsToJson());
	}
	
	public JSONObject itemToJson() throws JSONException {
		return new JSONObject().put("id", id)
				.put("name", name)
				.put("Description", itemDesc);
	}
	
	public JSONArray roomsToJson() throws JSONException {
		Iterator<ItemRoom> rooms = this.itemRoom.iterator();
		JSONArray jsonRoom = new JSONArray();
		while (rooms.hasNext()) {
			Room room = rooms.next().getRoom();
			jsonRoom.put(room.roomToJson());
		}
		return jsonRoom;
	}
	
}