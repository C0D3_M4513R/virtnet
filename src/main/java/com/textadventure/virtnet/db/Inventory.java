package com.textadventure.virtnet.db;


import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Generated;
import javax.persistence.*;

@Entity
@Getter @Setter @RequiredArgsConstructor  @AllArgsConstructor @NoArgsConstructor
public class Inventory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="User_ID")
	@NonNull
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "itemRoomTriggerEvent_ID")
	@NonNull
	private ItemRoom itemRoom;
	
}
